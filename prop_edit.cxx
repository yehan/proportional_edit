#include <cassert>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "vtkPLYReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkProportionalEditFilter.h"

/*
 * \brief Read a surface mesh
 */
vtkPolyData* readSurfaceMesh(const std::string& fname);

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " file name (.vtk)" << std::endl;
        return EXIT_FAILURE;
    }

    /*
     * read mesh file
     */
    vtkPolyData* surfMesh = readSurfaceMesh(argv[1]);
    if (surfMesh == nullptr)
    {
        std::cerr << "failed to open mesh file " << argv[1] << std::endl;
    }

    vtkNew<vtkProportionalEditFilter> propEdit;
    propEdit->SetInfluenceRadius(0.05);
    propEdit->SetProjected(true);
    propEdit->SetProjectionDirection(1.0, 0.0, 0.0);
    // propEdit->SetFalloff("Linear");
    // propEdit->SetFalloff(vtkProportionalEditFilter::Smooth);
    propEdit->SetAnchorPointId(5888);
    propEdit->SetAnchorPointDisplacement(0.0, -0.0075, 0.0);
    propEdit->SetGenerateEditDistance(true);
    propEdit->SetGenerateEditVector(true);
    // propEdit->PassThroughCellIdsOn();
    propEdit->SetInputDataObject(surfMesh);
    propEdit->PrintSelf(std::cout, vtkIndent(4));
    propEdit->Update();
    vtkNew<vtkPolyData> editedSurf;
    editedSurf->ShallowCopy(propEdit->GetOutput());

    /*
     * write modified mesh file
     */
    vtkSmartPointer<vtkPolyDataWriter> writer         = vtkSmartPointer<vtkPolyDataWriter>::New();
    const std::string                  outputFilename = "edited_mesh.vtk";
    writer->SetFileName(outputFilename.c_str());
    writer->SetInputData(editedSurf);
    writer->Write();

    return EXIT_SUCCESS;
}

vtkPolyData* readSurfaceMesh(const std::string& fname)
{
    auto         dotPos = fname.find_last_of(".");
    std::string  fileExt(fname.begin() + dotPos + 1, fname.end());
    vtkPolyData* surfMesh = vtkPolyData::New();

    if (fileExt == "ply")
    {
        vtkSmartPointer<vtkPLYReader> reader = vtkSmartPointer<vtkPLYReader>::New();
        reader->SetFileName(fname.c_str());
        reader->Update();
        surfMesh->ShallowCopy(reader->GetOutput());
    }
    else if (fileExt == "vtk")
    {
        vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
        reader->SetFileName(fname.c_str());
        reader->Update();
        surfMesh->ShallowCopy(reader->GetOutput());
    }

    return surfMesh;
}


